//
//  Webservice.swift
//  The Appraisal Lane
//
//  Created by Vinícius Brito on 01/03/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Webservice {

    class func getData(field: String, completion: @escaping (Bool, JSON?) -> ()) {
    
        let url = field
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON {
            (response) in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    completion(json.dictionary != nil, json)
                }
                
            case .failure(let error):
                print("Webservice Error: \(error)")
            }
        }
        
    }

}
