//
//  InitialCell.swift
//  The Appraisal Lane
//
//  Created by Vinícius Brito on 02/03/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit
import Kingfisher

class InitialCell: UITableViewCell {
    
    static var reuseIdentifier = String(describing: InitialCell.self)
    static var nib = UINib(nibName: InitialCell.reuseIdentifier, bundle: nil)
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    
    var pictures = [Pictures]()
    var name = [Name]()
    var dob = [Dob]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(data: DataModel) {
        
        pictures = data.pictures
        for item in pictures {
            if let url = URL(string: item.medium ) {
                photoImageView.kf.setImage(with: url)
            } else {
                photoImageView.image = UIImage(named: "icon-avatar")
            }
            photoImageView.layer.cornerRadius = photoImageView.frame.height / 2
        }
        
        name = data.name
        for item in name {
            nameLabel.text = String(format:"\(item.title) \(item.first) \(item.last)")   
        }
        
        dob = data.dob
        for item in dob {
            dateOfBirthLabel.text = String(format:"\(item.date)")
        }
    }
}
