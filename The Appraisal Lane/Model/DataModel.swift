//
//  DataModel.swift
//  The Appraisal Lane
//
//  Created by Vinícius Brito on 01/03/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit
import SwiftyJSON

class DataModel: NSObject {
    
    var pictures: [Pictures] = []
    var name: [Name] = []
    var dob: [Dob] = []
    var email : String = ""
    var phone : String = ""
    var location: [Location] = []
    var login: [Login] = []
    
    // MARK: - Parse JSON
    static func parseList(json: JSON) -> [DataModel] {
        var list = [DataModel]()
        
        for oneElement in json["results"].arrayValue {
            let dataModel = DataModel.parse(json: oneElement)
            list.append(dataModel)
        }
        
        return list
    }
    
    static func parse(json: JSON) -> DataModel {
        let dataModel = DataModel()
        
        dataModel.pictures = Pictures.parse(json: json["picture"])
        dataModel.name = Name.parse(json: json["name"])
        dataModel.dob = Dob.parse(json: json["dob"])
        dataModel.email = json["email"].stringValue
        dataModel.phone = json["phone"].stringValue
        dataModel.location = Location.parse(json: json["location"])
        dataModel.login = Login.parse(json: json["login"])
        
        return dataModel
    }
    
}

// MARK: - Picture
class Pictures: NSObject {
    
    var thumbnail: String = ""
    var medium: String = ""
    var large: String = ""
    
    static func parse(json: JSON) -> [Pictures] {
        
        let pictures = Pictures()
        
        pictures.thumbnail = json["thumbnail"].stringValue
        pictures.medium = json["medium"].stringValue
        pictures.large = json["large"].stringValue
    
        return [pictures]
    }
}

// MARK: - Name
class Name: NSObject {
    
    var last : String = ""
    var first : String = ""
    var title : String = ""
    
    static func parse(json: JSON) -> [Name] {
        
        let name = Name()
        
        name.last = json["last"].stringValue
        name.first = json["first"].stringValue
        name.title = json["title"].stringValue
    
        return [name]
    }
}

// MARK: - Login
class Login: NSObject {
    
    var password: String = ""
    var username: String = ""
    
    static func parse(json: JSON) -> [Login] {
        
        let login = Login()
        
        login.password = json["password"].stringValue
        login.username = json["username"].stringValue
    
        return [login]
    }
}

// MARK: - Location
class Location: NSObject {
    
    var city: String = ""
    var state: String = ""
    var country: String = ""
    var street: [Street] = []
    
    static func parse(json: JSON) -> [Location] {
        
        let location = Location()
        
        location.city = json["city"].stringValue
        location.state = json["state"].stringValue
        location.country = json["country"].stringValue
        location.street = Street.parse(json: json["street"])
        
        return [location]
    }
    
}

// MARK: - Street
class Street: NSObject {
    
    var number: String = ""
    var name: String = ""
    
    static func parse(json: JSON) -> [Street] {
        
        let street = Street()
        
        street.number = json["number"].stringValue
        street.name = json["name"].stringValue
    
        return [street]
    }
}

// MARK: - Registered
class Dob: NSObject {
    
    var date: String = ""
    var age: String = ""
    
    static func parse(json: JSON) -> [Dob] {
        
        let dob = Dob()
        
        var convertedDate: String = ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newDateFormatter = DateFormatter()
        newDateFormatter.dateFormat = "MMM-d-yyyy"
        
        let dateTime = json["date"].stringValue
        let dateComponents = dateTime.components(separatedBy: "T")
        
        let splitDate = dateComponents[0]
        
        if let date = dateFormatter.date(from: splitDate) {
            convertedDate = newDateFormatter.string(from: date)
        }
        
        dob.date = convertedDate
        dob.age = json["age"].stringValue
    
        return [dob]
    }
}
