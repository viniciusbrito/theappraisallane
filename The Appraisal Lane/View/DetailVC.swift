//
//  DetailVC.swift
//  The Appraisal Lane
//
//  Created by Vinícius Brito on 02/03/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit

protocol DetailVCDelegate: class {
    func doSomethingOnInitialVC(data: String)
}

class DetailVC: UIViewController {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sendDataBackBtn: UIButton!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    var pictures = [Pictures]()
    var name = [Name]()
    var dob = [Dob]()
    var location = [Location]()
    var street = [Street]()
    var login = [Login]()
    var data = DataModel()
    var phoneNumber: String = ""
    
    weak var delegate: DetailVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup(data: data)
    }
    
    func setup(data: DataModel) {
        
        pictures = data.pictures
        for item in pictures {
            if let url = URL(string: item.large ) {
                photoImageView.kf.setImage(with: url)
            } else {
                photoImageView.image = UIImage(named: "icon-avatar")
            }
            photoImageView.layer.cornerRadius = photoImageView.frame.height / 2
        }
        
        name = data.name
        for item in name {
            self.title = String(format:"\(item.first) \(item.last)")
            nameLabel.text = String(format:"\(item.title) \(item.first) \(item.last)")
        }
        
        dob = data.dob
        for item in dob {
            dateOfBirthLabel.text = String(format:"\(item.date)")
        }
        
        emailLabel.text = data.email
        phoneButton.setTitle(data.phone, for: .normal)
        self.phoneNumber = data.phone
        
        location = data.location
        for item in location {
            street = item.street
            for i in street {
                addressLabel.text = String(format:"\(i.number), \(i.name) \(item.city) - \(item.state) - \(item.country)")
            }
        }
        
        login = data.login
        for item in login {
            usernameLabel.text = item.username
            passwordLabel.text = item.password
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func phoneButtonClick(_ sender: Any) {
        self.phoneNumber.makeACall()
    }
    
    @IBAction func sendDataBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        delegate?.doSomethingOnInitialVC(data: "send data from DetailVC to InitialVC through delegate")
    }
    
}

extension String {

    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }

    func isValid(regex: RegularExpressions) -> Bool { return isValid(regex: regex.rawValue) }
    func isValid(regex: String) -> Bool { return range(of: regex, options: .regularExpression) != nil }

    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter { CharacterSet.decimalDigits.contains($0) }
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }

    func makeACall() {
        guard   isValid(regex: .phone),
                let url = URL(string: "tel://\(self.onlyDigits())"),
                UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
