//
//  InitialVC.swift
//  The Appraisal Lane
//
//  Created by Vinícius Brito on 01/03/20.
//  Copyright © 2020 Vinícius Brito. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class InitialVC: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var getDataBtn: UIButton!
    @IBOutlet weak var initialTblView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Properties
    var name = String()
    //var dataFromJSON = DataModel()
    var results = Array<Any>()
    var list = [DataModel]()
    
    var identifier = "InitialCell"
    var detailIdentifier = "DetailVC"
    var storyBoardName = "Main"
    var navTitle = "InitialVC"
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView(tableView: initialTblView)
    }
    
    // MARK: - Methods
    func configTableView(tableView: UITableView) {
        self.title = navTitle
        activityIndicator.isHidden = true
        initialTblView.isHidden = true
        initialTblView.register(UINib(nibName: "InitialCell", bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func successSettings() {
        self.initialTblView.isHidden = false
        self.activityIndicator.stopAnimating()
    }
    
    // MARK: - <IBAction>
    @IBAction func getDataBtnClick(_ sender: Any) {
        
        self.activityIndicator.startAnimating()
        
        Webservice.getData(field: "https://randomuser.me/api/?results=30") { (success, json) in
            
            guard let json = json, success == true else {
                self.activityIndicator.stopAnimating()
                return
            }
            
            let employeeList = DataModel.parseList(json: json)
            self.list = employeeList
            
            self.successSettings()
            self.initialTblView.reloadData()
        }
        
    }
    
}

// MARK: - <TableView>
extension InitialVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height: CGFloat = 70.0
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? InitialCell {
            let dataModel = self.list[indexPath.row]
            cell.setup(data: dataModel)
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        initialTblView.deselectRow(at: indexPath, animated: true)
        if let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailVC") as? DetailVC {
            let dataModel = self.list[indexPath.row]
            detailVC.delegate = self
            detailVC.data = dataModel
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
}

// MARK: - <DetailVCDelegate>
extension InitialVC: DetailVCDelegate {
    func doSomethingOnInitialVC(data: String) {
        alertBack(message: data)
    }
    
    func alertBack(message: String) {
        let alert = UIAlertController(title: "Yay",
                                      message: "Delegation: \(message)",
                                      preferredStyle: .alert)
        let woohoo = UIAlertAction(title: "WhooHoo!",
                                   style: .default) { (UIAlertAction) in
                                    
        }
        alert.addAction(woohoo)
        self.present(alert, animated: true)
    }
}
